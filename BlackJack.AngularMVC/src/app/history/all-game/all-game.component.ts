import { Component, OnInit } from '@angular/core';
import { GetAllGamesHistoryView } from 'src/app/shared/views/history-views/get-all-games-history.view';
import { HistoryService } from 'src/app/shared/services/history.service';
import { Router } from '@angular/router';
import { GameService } from '../../shared/services/game.service';

@Component({
  selector: 'app-all-game',
  templateUrl: './all-game.component.html',
  styleUrls: ['./all-game.component.css']
})

export class AllGameComponent implements OnInit {
    constructor(private historyService: HistoryService, private gameService: GameService, private route: Router) { }
    ngOnInit() {
        this.getAllGames();
    }
    
    public allgames: GetAllGamesHistoryView;

    getAllGames(): void {
        this.historyService.getAllGames()
            .subscribe(games => {
                this.allgames = games
            });
    }
    showDetails(gameId: number) {
        this.route.navigate(['allGames/gameDetails', gameId])         
    }
}
