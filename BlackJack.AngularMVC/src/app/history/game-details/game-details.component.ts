import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HistoryService } from 'src/app/shared/services/history.service';
import { GetGameDetailsHistoryView } from 'src/app/shared/views/history-views/get-game-details-history.view';
import { GameService } from '../../shared/services/game.service';

@Component({
    selector: 'app-game-details',
    templateUrl: './game-details.component.html',
    styleUrls: ['./game-details.component.css']
})

export class GameDetailsComponent implements OnInit {

    private gameId: number;
    constructor(private historyService: HistoryService, private gameService: GameService, private activateRoute: ActivatedRoute) {
        this.gameId = this.activateRoute.snapshot.params['id'];
    }

    ngOnInit() {
        this.getGameDetails(this.gameId);
    }

    gameDetails: GetGameDetailsHistoryView;
    getGameDetails(gameId: number) {
        this.historyService.getGameDetails(gameId)
            .subscribe(gameDetails => {
                this.gameDetails = gameDetails
            });

    }
}
