import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/start',
        pathMatch: 'full'
    },
    {
        path: 'start',
        loadChildren: 'src/app/game/start-game/start-game.module#StartGameModule'
        
    },
    {
        path: 'currentGame/:id/:getCardEnum',
        loadChildren: 'src/app/game/current-game/current-game.module#CurrentGameModule'
    },
    {
        path: 'endGame/:id',
        loadChildren: 'src/app/game/end-game/end-game.module#EndGameModule'
    },
    {
        path: 'allGames',
        loadChildren: 'src/app/history/history.module#HistoryModule'
    }

];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
