import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetStartGameView } from 'src/app/shared/views/game-views/get-start-game.view';
import { PlayCurrentGameGameView } from 'src/app/shared/views/game-views/play-current-game-game.view';
import { GetEndGameGameView } from 'src/app/shared/views/game-views/get-end-game-game.view';
import { CreateGameGameView } from 'src/app/shared/views/game-views/create-game-game.view'
import { environment } from 'src/environments/environment';
import { GetCardEnum } from '../views/enum-views/get-card-enum.view';
import { RoleEnumView } from '../views/enum-views/role-enum.view';
import { PlayerStatusEnumView } from '../views/enum-views/player-status-enum.view';
import { CardSuitEnumView } from '../views/enum-views/card-suit-enum.view';
import { CardNameEnumView } from '../views/enum-views/card-name-enum.view';
import { Router } from '@angular/router';
@Injectable()

export class GameService {

    private gameAPIControllerUrl = environment.gameAPIControllerUrl;
    constructor(private http: HttpClient, private route: Router) { }

    public getStart(): Observable<GetStartGameView> {
        return this.http.get<GetStartGameView>(this.gameAPIControllerUrl + "GetStart");
    }

    public createGame(createGameGameView: CreateGameGameView): Observable<number> {
        if (createGameGameView.currentPlayer == undefined) {
            createGameGameView.currentPlayer = "";
        }
        const body = { currentPlayer: createGameGameView.currentPlayer, botsCount: createGameGameView.botsCount };
        return this.http.post<number>(this.gameAPIControllerUrl + "CreateGame", body);
    }

    public playCurrentGame(gameId: number, getCardEnum: GetCardEnum): Observable<PlayCurrentGameGameView> {
        return this.http.get<PlayCurrentGameGameView>(this.gameAPIControllerUrl + "PlayCurrentGame/" + gameId + "/" + getCardEnum);
    }

    public getEndGame(gameId: number): Observable<GetEndGameGameView> {
        return this.http.get<GetEndGameGameView>(this.gameAPIControllerUrl + "GetEndGame/" + gameId);
    }

    public getTypeRole(id: number): any {
        return RoleEnumView[id];
    }

    public getTypeCardEnum(id: number): any {
        return GetCardEnum[id];
    }

    public getTypePlayerStatus(id: number): any {
        return PlayerStatusEnumView[id];
    }

    public getTypeCardSuit(id: number): any {
        return CardSuitEnumView[id];
    }

    public getTypeCardName(id: number): any {
        return CardNameEnumView[id];
    }

    public startNewGame() {
        this.route.navigate(['/start']);
    }

    public openHistory() {
        this.route.navigate(['/allGames']);
    }
}
