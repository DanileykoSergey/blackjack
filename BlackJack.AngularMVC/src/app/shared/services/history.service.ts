import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetAllGamesHistoryView } from 'src/app/shared/views/history-views/get-all-games-history.view';
import { GetGameDetailsHistoryView } from 'src/app/shared/views/history-views/get-game-details-history.view';
import { environment } from 'src/environments/environment';

@Injectable()
export class HistoryService {

    private historyAPIControllerUrl = environment.historyAPIControllerUrl;
    constructor(private http: HttpClient) { }

    public getAllGames(): Observable<GetAllGamesHistoryView> {
        return this.http.get<GetAllGamesHistoryView>(this.historyAPIControllerUrl + "GetAllGames");
    }

    public getGameDetails(gameId: number): Observable<GetGameDetailsHistoryView> {
        return this.http.get<GetGameDetailsHistoryView>(this.historyAPIControllerUrl + "GetGameDetails/" + gameId);
    }   
}
