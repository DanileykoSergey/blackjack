﻿export enum GameEndEnumView {
    None = 0,
    EndGame = 1,
    ContinueGame = 2
}