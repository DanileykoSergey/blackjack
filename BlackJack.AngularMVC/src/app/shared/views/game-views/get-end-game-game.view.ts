﻿import { PlayerStatusEnumView } from "src/app/shared/views/enum-views/player-status-enum.view";
import { RoleEnumView } from "src/app/shared/views/enum-views/role-enum.view";
import { GameEndEnumView } from "src/app/shared/views/enum-views/game-end-enum.view";
import { CardSuitEnumView } from "src/app/shared/views/enum-views/card-suit-enum.view";
import { CardNameEnumView } from "src/app/shared/views/enum-views/card-name-enum.view";

export class GetEndGameGameView {
    public players: Array<PlayerGetEndGameGameViewItem>;
    public dealerPlayer: PlayerGetEndGameGameViewItem;
    public checkEndGame: GameEndEnumView;
    public gameId: number;
}

export class PlayerGetEndGameGameViewItem {
    public id: number;
    public name: string;
    public cardSum: number;
    public playerStatus: PlayerStatusEnumView;
    public role: RoleEnumView;
    public playerCards: Array<CardGetEndGameGameViewItem>;
}

export class CardGetEndGameGameViewItem {
    public id: number;
    public name: CardNameEnumView;
    public points: number;
    public suit: CardSuitEnumView;
}