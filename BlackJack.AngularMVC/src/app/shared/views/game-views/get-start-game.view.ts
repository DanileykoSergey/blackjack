﻿export class GetStartGameView
{
    public humanPlayers: PlayerGetStartGameViewItem[];    
}
export class PlayerGetStartGameViewItem
{
    public id: number;
    public name: string;     
}