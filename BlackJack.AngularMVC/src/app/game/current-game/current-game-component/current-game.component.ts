import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlayCurrentGameGameView } from 'src/app/shared/views/game-views/play-current-game-game.view';
import { GameService } from 'src/app/shared/services/game.service';
import { GameEndEnumView } from 'src/app/shared/views/enum-views/game-end-enum.view';
import { GetCardEnum } from 'src/app/shared/views/enum-views/get-card-enum.view';


@Component({
    selector: 'app-current-game',
    templateUrl: './current-game.component.html',
    styleUrls: ['./current-game.component.css']
})

export class CurrentGameComponent implements OnInit {
    private gameId: number;
    private getCardEnum: GetCardEnum;
    constructor(private gameService: GameService, private activateRoute: ActivatedRoute, private route: Router) {
        this.gameId = this.activateRoute.snapshot.params['id'];
        this.getCardEnum = this.activateRoute.snapshot.params['getCardEnum'];
    }
    ngOnInit() {
        this.createFirstRound(this.gameId, this.getCardEnum);
    }
     
    currentGame: PlayCurrentGameGameView;
    createFirstRound(gameId: number, getCardEnum: GetCardEnum) {
        this.gameService.playCurrentGame(gameId, getCardEnum)
            .subscribe(currentGame => {
                this.currentGame = currentGame
                if (currentGame.checkEndGame == GameEndEnumView.EndGame) {
                    this.endGame();
                }
            });

    }

    createNextRound(getCardEnumId: number) {
        this.gameService.playCurrentGame(this.gameId, this.gameService.getTypeCardEnum(getCardEnumId))
            .subscribe(nextRound => {
                this.currentGame = nextRound
                if (nextRound.checkEndGame == GameEndEnumView.EndGame) {
                    this.endGame();
                }
            });
    }

    endGame() {
                this.route.navigate(['/endGame', this.gameId])        
    }

}
