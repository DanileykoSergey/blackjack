import { NgModule } from '@angular/core';
import { CurrentGameRoutingModule } from './current-game-routing.module';
import { CurrentGameComponent } from 'src/app/Game/current-game/current-game-component/current-game.component';
import { SharedModule } from 'src/app/shared/modules/shared.module';

@NgModule({
    imports: [
        SharedModule,
        CurrentGameRoutingModule,
    ],
    declarations: [CurrentGameComponent]
})

export class CurrentGameModule { }
