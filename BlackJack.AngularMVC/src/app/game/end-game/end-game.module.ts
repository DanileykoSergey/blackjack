import { NgModule } from '@angular/core';
import { EndGameComponent } from 'src/app/Game/end-game/end-game-component/end-game.component';
import { EndGameRoutingModule } from './end-game-routing.module';
import { SharedModule } from 'src/app/shared/modules/shared.module';

@NgModule({
    imports: [
        SharedModule,
        EndGameRoutingModule
    ],
    declarations: [EndGameComponent]
})

export class EndGameModule { }
