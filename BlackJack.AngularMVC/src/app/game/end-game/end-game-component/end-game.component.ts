import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { GameService } from 'src/app/shared/services/game.service';
import { GetEndGameGameView } from 'src/app/shared/views/game-views/get-end-game-game.view';


@Component({
    selector: 'app-end-game',
    templateUrl: './end-game.component.html',
    styleUrls: ['./end-game.component.css']
})

export class EndGameComponent implements OnInit {

    private gameId: number;
    constructor(private gameService: GameService, private activateRoute: ActivatedRoute) {
        this.gameId = this.activateRoute.snapshot.params['id'];
    }

    ngOnInit() {
        this.endGame(this.gameId);
    }
    endGameInfo: GetEndGameGameView;

    endGame(gameId:number) {
        this.gameService.getEndGame(gameId)
            .subscribe(endGame =>
                this.endGameInfo = endGame
            );
    }
}
