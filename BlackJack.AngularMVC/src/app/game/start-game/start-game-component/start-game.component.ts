﻿import { Component, OnInit } from '@angular/core';
import { GameService } from 'src/app/shared/services/game.service';
import { CreateGameGameView } from 'src/app/shared/views/game-views/create-game-game.view'
import { Router } from '@angular/router';
import { error } from '@angular/compiler/src/util';
import { GetCardEnum } from 'src/app/shared/views/enum-views/get-card-enum.view';

@Component({
    selector: 'app-start',
    templateUrl: './start-game.component.html',
    styleUrls: ['./start-game.component.css']
})

export class StartGameComponent implements OnInit {

    public humanPlayers: string[] = [];
    public botsCount: Array<number> = [1, 2, 3, 4, 5];
    constructor(private gameService: GameService, private route: Router) { }

    ngOnInit() {
        this.start();
    }

    start(): void {
        this.gameService.getStart()
            .subscribe(heroes => {
                for (var i = 0; i < heroes.humanPlayers.length; i++) {
                    this.humanPlayers[i] = heroes.humanPlayers[i].name
                }              
            });
    }

    createGameGameView: CreateGameGameView = new CreateGameGameView();
    gameId: number;
    getCardEnum = GetCardEnum.Start;
    
    createGame(createGameGameView: CreateGameGameView) {
        this.gameService.createGame(createGameGameView).subscribe(result => {
            this.gameId = result;
            this.route.navigate(['/currentGame',  this.gameId, this.getCardEnum ])
        });
    }
}
