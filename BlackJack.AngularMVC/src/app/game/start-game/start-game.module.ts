import { NgModule } from '@angular/core';
import { StartGameRoutingModule } from './start-game-routing.module';
import { StartGameComponent } from 'src/app/Game/start-game/start-game-component/start-game.component';
import { SharedModule } from 'src/app/shared/modules/shared.module';

@NgModule({
    imports: [
        SharedModule,
        StartGameRoutingModule,
      ],
    declarations: [StartGameComponent]
})

export class StartGameModule { }
