export const environment = {
    production: false,
    gameAPIControllerUrl: 'http://localhost:58816/api/Game/',
    historyAPIControllerUrl: 'http://localhost:58816/api/History/'
};
