"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var environment_1 = require("src/environments/environment");
var get_card_enum_view_1 = require("../views/enum-views/get-card-enum.view");
var role_enum_view_1 = require("../views/enum-views/role-enum.view");
var player_status_enum_view_1 = require("../views/enum-views/player-status-enum.view");
var card_suit_enum_view_1 = require("../views/enum-views/card-suit-enum.view");
var card_name_enum_view_1 = require("../views/enum-views/card-name-enum.view");
var router_1 = require("@angular/router");
var GameService = /** @class */ (function () {
    function GameService(http, route) {
        this.http = http;
        this.route = route;
        this.gameAPIControllerUrl = environment_1.environment.gameAPIControllerUrl;
    }
    GameService.prototype.getStart = function () {
        return this.http.get(this.gameAPIControllerUrl + "GetStart");
    };
    GameService.prototype.createGame = function (createGameGameView) {
        if (createGameGameView.currentPlayer == undefined) {
            createGameGameView.currentPlayer = "";
        }
        var body = { currentPlayer: createGameGameView.currentPlayer, botsCount: createGameGameView.botsCount };
        return this.http.post(this.gameAPIControllerUrl + "CreateGame", body);
    };
    GameService.prototype.playCurrentGame = function (gameId, getCardEnum) {
        return this.http.get(this.gameAPIControllerUrl + "PlayCurrentGame/" + gameId + "/" + getCardEnum);
    };
    GameService.prototype.getEndGame = function (gameId) {
        return this.http.get(this.gameAPIControllerUrl + "GetEndGame/" + gameId);
    };
    GameService.prototype.getTypeRole = function (id) {
        return role_enum_view_1.RoleEnumView[id];
    };
    GameService.prototype.getTypeCardEnum = function (id) {
        return get_card_enum_view_1.GetCardEnum[id];
    };
    GameService.prototype.getTypePlayerStatus = function (id) {
        return player_status_enum_view_1.PlayerStatusEnumView[id];
    };
    GameService.prototype.getTypeCardSuit = function (id) {
        return card_suit_enum_view_1.CardSuitEnumView[id];
    };
    GameService.prototype.getTypeCardName = function (id) {
        return card_name_enum_view_1.CardNameEnumView[id];
    };
    GameService.prototype.startNewGame = function () {
        this.route.navigate(['/start']);
    };
    GameService.prototype.openHistory = function () {
        this.route.navigate(['/allGames']);
    };
    GameService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, router_1.Router])
    ], GameService);
    return GameService;
}());
exports.GameService = GameService;
//# sourceMappingURL=game.service.js.map