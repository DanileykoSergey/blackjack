"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CardSuitEnumView;
(function (CardSuitEnumView) {
    CardSuitEnumView[CardSuitEnumView["None"] = 0] = "None";
    CardSuitEnumView[CardSuitEnumView["Diamonds"] = 1] = "Diamonds";
    CardSuitEnumView[CardSuitEnumView["Hearts"] = 2] = "Hearts";
    CardSuitEnumView[CardSuitEnumView["Spades"] = 3] = "Spades";
    CardSuitEnumView[CardSuitEnumView["Clubs"] = 4] = "Clubs";
})(CardSuitEnumView = exports.CardSuitEnumView || (exports.CardSuitEnumView = {}));
//# sourceMappingURL=card-suit-enum.view.js.map