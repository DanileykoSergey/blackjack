"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CardNameEnumView;
(function (CardNameEnumView) {
    CardNameEnumView[CardNameEnumView["None"] = 0] = "None";
    CardNameEnumView[CardNameEnumView["Two"] = 1] = "Two";
    CardNameEnumView[CardNameEnumView["Three"] = 2] = "Three";
    CardNameEnumView[CardNameEnumView["Four"] = 3] = "Four";
    CardNameEnumView[CardNameEnumView["Five"] = 4] = "Five";
    CardNameEnumView[CardNameEnumView["Six"] = 5] = "Six";
    CardNameEnumView[CardNameEnumView["Seven"] = 6] = "Seven";
    CardNameEnumView[CardNameEnumView["Eight"] = 7] = "Eight";
    CardNameEnumView[CardNameEnumView["Nine"] = 8] = "Nine";
    CardNameEnumView[CardNameEnumView["Ten"] = 9] = "Ten";
    CardNameEnumView[CardNameEnumView["Jack"] = 10] = "Jack";
    CardNameEnumView[CardNameEnumView["Queen"] = 11] = "Queen";
    CardNameEnumView[CardNameEnumView["King"] = 12] = "King";
    CardNameEnumView[CardNameEnumView["Ace"] = 13] = "Ace";
})(CardNameEnumView = exports.CardNameEnumView || (exports.CardNameEnumView = {}));
//# sourceMappingURL=card-name-enum.view.js.map