"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameEndEnumView;
(function (GameEndEnumView) {
    GameEndEnumView[GameEndEnumView["None"] = 0] = "None";
    GameEndEnumView[GameEndEnumView["EndGame"] = 1] = "EndGame";
    GameEndEnumView[GameEndEnumView["ContinueGame"] = 2] = "ContinueGame";
})(GameEndEnumView = exports.GameEndEnumView || (exports.GameEndEnumView = {}));
//# sourceMappingURL=game-end-enum.view.js.map