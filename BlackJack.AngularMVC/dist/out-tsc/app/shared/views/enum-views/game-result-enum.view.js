"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameResultEnumView;
(function (GameResultEnumView) {
    GameResultEnumView[GameResultEnumView["None"] = 0] = "None";
    GameResultEnumView[GameResultEnumView["InGame"] = 1] = "InGame";
    GameResultEnumView[GameResultEnumView["Winner"] = 2] = "Winner";
    GameResultEnumView[GameResultEnumView["Looser"] = 3] = "Looser";
    GameResultEnumView[GameResultEnumView["Draw"] = 4] = "Draw";
    GameResultEnumView[GameResultEnumView["BlackJack"] = 5] = "BlackJack";
})(GameResultEnumView = exports.GameResultEnumView || (exports.GameResultEnumView = {}));
//# sourceMappingURL=game-result-enum.view.js.map