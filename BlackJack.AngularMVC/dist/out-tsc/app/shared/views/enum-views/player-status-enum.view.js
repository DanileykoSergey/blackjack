"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PlayerStatusEnumView;
(function (PlayerStatusEnumView) {
    PlayerStatusEnumView[PlayerStatusEnumView["None"] = 0] = "None";
    PlayerStatusEnumView[PlayerStatusEnumView["InGame"] = 1] = "InGame";
    PlayerStatusEnumView[PlayerStatusEnumView["Winner"] = 2] = "Winner";
    PlayerStatusEnumView[PlayerStatusEnumView["Looser"] = 3] = "Looser";
    PlayerStatusEnumView[PlayerStatusEnumView["Draw"] = 4] = "Draw";
    PlayerStatusEnumView[PlayerStatusEnumView["BlackJack"] = 5] = "BlackJack";
})(PlayerStatusEnumView = exports.PlayerStatusEnumView || (exports.PlayerStatusEnumView = {}));
//# sourceMappingURL=player-status-enum.view.js.map