"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetCardEnum;
(function (GetCardEnum) {
    GetCardEnum[GetCardEnum["None"] = 0] = "None";
    GetCardEnum[GetCardEnum["Start"] = 1] = "Start";
    GetCardEnum[GetCardEnum["Take"] = 2] = "Take";
})(GetCardEnum = exports.GetCardEnum || (exports.GetCardEnum = {}));
//# sourceMappingURL=get-card-enum.js.map