"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var game_service_1 = require("src/app/shared/services/game.service");
var create_game_game_view_1 = require("src/app/shared/views/game-views/create-game-game.view");
var router_1 = require("@angular/router");
var get_card_enum_view_1 = require("src/app/shared/views/enum-views/get-card-enum.view");
var StartGameComponent = /** @class */ (function () {
    function StartGameComponent(gameService, route) {
        this.gameService = gameService;
        this.route = route;
        this.humanPlayers = [];
        this.botsCount = [1, 2, 3, 4, 5];
        this.createGameGameView = new create_game_game_view_1.CreateGameGameView();
        this.getCardEnum = get_card_enum_view_1.GetCardEnum.Start;
    }
    StartGameComponent.prototype.ngOnInit = function () {
        this.start();
    };
    StartGameComponent.prototype.start = function () {
        var _this = this;
        this.gameService.getStart()
            .subscribe(function (heroes) {
            for (var i = 0; i < heroes.humanPlayers.length; i++) {
                _this.humanPlayers[i] = heroes.humanPlayers[i].name;
            }
        });
    };
    StartGameComponent.prototype.createGame = function (createGameGameView) {
        var _this = this;
        this.gameService.createGame(createGameGameView).subscribe(function (result) {
            _this.gameId = result;
            _this.route.navigate(['/currentGame', _this.gameId, _this.getCardEnum]);
        });
    };
    StartGameComponent = __decorate([
        core_1.Component({
            selector: 'app-start',
            templateUrl: './start-game.component.html',
            styleUrls: ['./start-game.component.css']
        }),
        __metadata("design:paramtypes", [game_service_1.GameService, router_1.Router])
    ], StartGameComponent);
    return StartGameComponent;
}());
exports.StartGameComponent = StartGameComponent;
//# sourceMappingURL=start-game.component.js.map