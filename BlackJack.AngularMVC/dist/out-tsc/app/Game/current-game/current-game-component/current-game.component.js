"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var game_service_1 = require("src/app/shared/services/game.service");
var game_end_enum_view_1 = require("src/app/shared/views/enum-views/game-end-enum.view");
var CurrentGameComponent = /** @class */ (function () {
    function CurrentGameComponent(gameService, activateRoute, route) {
        this.gameService = gameService;
        this.activateRoute = activateRoute;
        this.route = route;
        this.gameId = this.activateRoute.snapshot.params['id'];
        this.getCardEnum = this.activateRoute.snapshot.params['getCardEnum'];
    }
    CurrentGameComponent.prototype.ngOnInit = function () {
        this.createFirstRound(this.gameId, this.getCardEnum);
    };
    CurrentGameComponent.prototype.createFirstRound = function (gameId, getCardEnum) {
        var _this = this;
        this.gameService.playCurrentGame(gameId, getCardEnum)
            .subscribe(function (currentGame) {
            _this.currentGame = currentGame;
            if (currentGame.checkEndGame == game_end_enum_view_1.GameEndEnumView.EndGame) {
                _this.endGame();
            }
        });
    };
    CurrentGameComponent.prototype.createNextRound = function (getCardEnumId) {
        var _this = this;
        this.gameService.playCurrentGame(this.gameId, this.gameService.getTypeCardEnum(getCardEnumId))
            .subscribe(function (nextRound) {
            _this.currentGame = nextRound;
            if (nextRound.checkEndGame == game_end_enum_view_1.GameEndEnumView.EndGame) {
                _this.endGame();
            }
        });
    };
    CurrentGameComponent.prototype.endGame = function () {
        this.route.navigate(['/endGame', this.gameId]);
    };
    CurrentGameComponent = __decorate([
        core_1.Component({
            selector: 'app-current-game',
            templateUrl: './current-game.component.html',
            styleUrls: ['./current-game.component.css']
        }),
        __metadata("design:paramtypes", [game_service_1.GameService, router_1.ActivatedRoute, router_1.Router])
    ], CurrentGameComponent);
    return CurrentGameComponent;
}());
exports.CurrentGameComponent = CurrentGameComponent;
//# sourceMappingURL=current-game.component.js.map