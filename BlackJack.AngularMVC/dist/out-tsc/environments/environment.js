"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false,
    gameAPIControllerUrl: 'http://localhost:58816/api/Game/',
    historyAPIControllerUrl: 'http://localhost:58816/api/History/'
};
//# sourceMappingURL=environment.js.map