(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["src-app-game-start-game-start-game-module"],{

/***/ "./src/app/Game/start-game/start-game-component/start-game.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/Game/start-game/start-game-component/start-game.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Game/start-game/start-game-component/start-game.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/Game/start-game/start-game-component/start-game.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Welcome to the game</h1>\r\n<h4>Enter your name or select player</h4>\r\n<kendo-combobox [(ngModel)]=\"createGameGameView.currentPlayer\"\r\n                [data]=\"humanPlayers\"\r\n                [allowCustom]=\"true\">\r\n</kendo-combobox>\r\n<h4>Choose count bot-player</h4>\r\n<kendo-dropdownlist [data]=\"botsCount\"\r\n                    [value]=\"0\"\r\n                    [(ngModel)]=\"createGameGameView.botsCount\">\r\n</kendo-dropdownlist>\r\n<h4>and start game</h4>\r\n<button kendoButton (click)=\"createGame(createGameGameView)\">Start</button>\r\n"

/***/ }),

/***/ "./src/app/Game/start-game/start-game-component/start-game.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/Game/start-game/start-game-component/start-game.component.ts ***!
  \******************************************************************************/
/*! exports provided: StartGameComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartGameComponent", function() { return StartGameComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_services_game_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/shared/services/game.service */ "./src/app/shared/services/game.service.ts");
/* harmony import */ var src_app_shared_views_game_views_create_game_game_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/views/game-views/create-game-game.view */ "./src/app/shared/views/game-views/create-game-game.view.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_shared_views_enum_views_get_card_enum_view__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/views/enum-views/get-card-enum.view */ "./src/app/shared/views/enum-views/get-card-enum.view.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var StartGameComponent = /** @class */ (function () {
    function StartGameComponent(gameService, route) {
        this.gameService = gameService;
        this.route = route;
        this.humanPlayers = [];
        this.botsCount = [1, 2, 3, 4, 5];
        this.createGameGameView = new src_app_shared_views_game_views_create_game_game_view__WEBPACK_IMPORTED_MODULE_2__["CreateGameGameView"]();
        this.getCardEnum = src_app_shared_views_enum_views_get_card_enum_view__WEBPACK_IMPORTED_MODULE_4__["GetCardEnum"].Start;
    }
    StartGameComponent.prototype.ngOnInit = function () {
        this.start();
    };
    StartGameComponent.prototype.start = function () {
        var _this = this;
        this.gameService.getStart()
            .subscribe(function (heroes) {
            for (var i = 0; i < heroes.humanPlayers.length; i++) {
                _this.humanPlayers[i] = heroes.humanPlayers[i].name;
            }
        });
    };
    StartGameComponent.prototype.createGame = function (createGameGameView) {
        var _this = this;
        this.gameService.createGame(createGameGameView).subscribe(function (result) {
            _this.gameId = result;
            _this.route.navigate(['/currentGame', _this.gameId, _this.getCardEnum]);
        });
    };
    StartGameComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-start',
            template: __webpack_require__(/*! ./start-game.component.html */ "./src/app/Game/start-game/start-game-component/start-game.component.html"),
            styles: [__webpack_require__(/*! ./start-game.component.css */ "./src/app/Game/start-game/start-game-component/start-game.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_shared_services_game_service__WEBPACK_IMPORTED_MODULE_1__["GameService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], StartGameComponent);
    return StartGameComponent;
}());



/***/ }),

/***/ "./src/app/game/start-game/start-game-routing.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/game/start-game/start-game-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: StartGameRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartGameRoutingModule", function() { return StartGameRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_Game_start_game_start_game_component_start_game_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Game/start-game/start-game-component/start-game.component */ "./src/app/Game/start-game/start-game-component/start-game.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: src_app_Game_start_game_start_game_component_start_game_component__WEBPACK_IMPORTED_MODULE_2__["StartGameComponent"]
    }
];
var StartGameRoutingModule = /** @class */ (function () {
    function StartGameRoutingModule() {
    }
    StartGameRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], StartGameRoutingModule);
    return StartGameRoutingModule;
}());



/***/ }),

/***/ "./src/app/game/start-game/start-game.module.ts":
/*!******************************************************!*\
  !*** ./src/app/game/start-game/start-game.module.ts ***!
  \******************************************************/
/*! exports provided: StartGameModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartGameModule", function() { return StartGameModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _start_game_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./start-game-routing.module */ "./src/app/game/start-game/start-game-routing.module.ts");
/* harmony import */ var src_app_Game_start_game_start_game_component_start_game_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Game/start-game/start-game-component/start-game.component */ "./src/app/Game/start-game/start-game-component/start-game.component.ts");
/* harmony import */ var src_app_shared_modules_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/modules/shared.module */ "./src/app/shared/modules/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var StartGameModule = /** @class */ (function () {
    function StartGameModule() {
    }
    StartGameModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                src_app_shared_modules_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"],
                _start_game_routing_module__WEBPACK_IMPORTED_MODULE_1__["StartGameRoutingModule"],
            ],
            declarations: [src_app_Game_start_game_start_game_component_start_game_component__WEBPACK_IMPORTED_MODULE_2__["StartGameComponent"]]
        })
    ], StartGameModule);
    return StartGameModule;
}());



/***/ }),

/***/ "./src/app/shared/views/game-views/create-game-game.view.ts":
/*!******************************************************************!*\
  !*** ./src/app/shared/views/game-views/create-game-game.view.ts ***!
  \******************************************************************/
/*! exports provided: CreateGameGameView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateGameGameView", function() { return CreateGameGameView; });
var CreateGameGameView = /** @class */ (function () {
    function CreateGameGameView() {
    }
    return CreateGameGameView;
}());



/***/ })

}]);
//# sourceMappingURL=src-app-game-start-game-start-game-module.js.map