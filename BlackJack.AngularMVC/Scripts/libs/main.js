(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"src/app/game/current-game/current-game.module": [
		"./src/app/game/current-game/current-game.module.ts",
		"src-app-game-current-game-current-game-module"
	],
	"src/app/game/end-game/end-game.module": [
		"./src/app/game/end-game/end-game.module.ts",
		"src-app-game-end-game-end-game-module"
	],
	"src/app/game/start-game/start-game.module": [
		"./src/app/game/start-game/start-game.module.ts",
		"src-app-game-start-game-start-game-module"
	],
	"src/app/history/history.module": [
		"./src/app/history/history.module.ts",
		"src-app-history-history-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error('Cannot find module "' + req + '".');
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return __webpack_require__.e(ids[1]).then(function() {
		var module = __webpack_require__(ids[0]);
		return module;
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        redirectTo: '/start',
        pathMatch: 'full'
    },
    {
        path: 'start',
        loadChildren: 'src/app/game/start-game/start-game.module#StartGameModule'
    },
    {
        path: 'currentGame/:id/:getCardEnum',
        loadChildren: 'src/app/game/current-game/current-game.module#CurrentGameModule'
    },
    {
        path: 'endGame/:id',
        loadChildren: 'src/app/game/end-game/end-game.module#EndGameModule'
    },
    {
        path: 'allGames',
        loadChildren: 'src/app/history/history.module#HistoryModule'
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header\">\r\n        <div class=\"links_container\">\r\n            <a routerLink=\"\" class=\"links\">Start game</a>\r\n            <a routerLink=\"/allGames\" class=\"links\">History</a>\r\n        </div>\r\n</div>\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! .//app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var src_app_shared_services_game_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/services/game.service */ "./src/app/shared/services/game.service.ts");
/* harmony import */ var src_app_shared_services_history_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared/services/history.service */ "./src/app/shared/services/history.service.ts");
/* harmony import */ var src_app_shared_modules_shared_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/shared/modules/shared.module */ "./src/app/shared/modules/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                src_app_shared_modules_shared_module__WEBPACK_IMPORTED_MODULE_9__["SharedModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            ],
            providers: [
                { provide: _angular_common__WEBPACK_IMPORTED_MODULE_4__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_4__["HashLocationStrategy"] },
                src_app_shared_services_game_service__WEBPACK_IMPORTED_MODULE_7__["GameService"],
                src_app_shared_services_history_service__WEBPACK_IMPORTED_MODULE_8__["HistoryService"]
            ],
            exports: [
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/shared/modules/shared.module.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/modules/shared.module.ts ***!
  \*************************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _progress_kendo_angular_buttons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @progress/kendo-angular-buttons */ "./node_modules/@progress/kendo-angular-buttons/dist/es/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @progress/kendo-angular-dropdowns */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/index.js");
/* harmony import */ var _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @progress/kendo-angular-grid */ "./node_modules/@progress/kendo-angular-grid/dist/es/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_5__["GridModule"],
                _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_4__["DropDownsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _progress_kendo_angular_buttons__WEBPACK_IMPORTED_MODULE_2__["ButtonsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
            ],
            declarations: [],
            exports: [
                _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_5__["GridModule"],
                _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_4__["DropDownsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _progress_kendo_angular_buttons__WEBPACK_IMPORTED_MODULE_2__["ButtonsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
            ]
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/app/shared/services/game.service.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/services/game.service.ts ***!
  \*************************************************/
/*! exports provided: GameService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameService", function() { return GameService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _views_enum_views_get_card_enum_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../views/enum-views/get-card-enum.view */ "./src/app/shared/views/enum-views/get-card-enum.view.ts");
/* harmony import */ var _views_enum_views_role_enum_view__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../views/enum-views/role-enum.view */ "./src/app/shared/views/enum-views/role-enum.view.ts");
/* harmony import */ var _views_enum_views_player_status_enum_view__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../views/enum-views/player-status-enum.view */ "./src/app/shared/views/enum-views/player-status-enum.view.ts");
/* harmony import */ var _views_enum_views_card_suit_enum_view__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../views/enum-views/card-suit-enum.view */ "./src/app/shared/views/enum-views/card-suit-enum.view.ts");
/* harmony import */ var _views_enum_views_card_name_enum_view__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../views/enum-views/card-name-enum.view */ "./src/app/shared/views/enum-views/card-name-enum.view.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var GameService = /** @class */ (function () {
    function GameService(http, route) {
        this.http = http;
        this.route = route;
        this.gameAPIControllerUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].gameAPIControllerUrl;
    }
    GameService.prototype.getStart = function () {
        return this.http.get(this.gameAPIControllerUrl + "GetStart");
    };
    GameService.prototype.createGame = function (createGameGameView) {
        if (createGameGameView.currentPlayer == undefined) {
            createGameGameView.currentPlayer = "";
        }
        var body = { currentPlayer: createGameGameView.currentPlayer, botsCount: createGameGameView.botsCount };
        return this.http.post(this.gameAPIControllerUrl + "CreateGame", body);
    };
    GameService.prototype.playCurrentGame = function (gameId, getCardEnum) {
        return this.http.get(this.gameAPIControllerUrl + "PlayCurrentGame/" + gameId + "/" + getCardEnum);
    };
    GameService.prototype.getEndGame = function (gameId) {
        return this.http.get(this.gameAPIControllerUrl + "GetEndGame/" + gameId);
    };
    GameService.prototype.getTypeRole = function (id) {
        return _views_enum_views_role_enum_view__WEBPACK_IMPORTED_MODULE_4__["RoleEnumView"][id];
    };
    GameService.prototype.getTypeCardEnum = function (id) {
        return _views_enum_views_get_card_enum_view__WEBPACK_IMPORTED_MODULE_3__["GetCardEnum"][id];
    };
    GameService.prototype.getTypePlayerStatus = function (id) {
        return _views_enum_views_player_status_enum_view__WEBPACK_IMPORTED_MODULE_5__["PlayerStatusEnumView"][id];
    };
    GameService.prototype.getTypeCardSuit = function (id) {
        return _views_enum_views_card_suit_enum_view__WEBPACK_IMPORTED_MODULE_6__["CardSuitEnumView"][id];
    };
    GameService.prototype.getTypeCardName = function (id) {
        return _views_enum_views_card_name_enum_view__WEBPACK_IMPORTED_MODULE_7__["CardNameEnumView"][id];
    };
    GameService.prototype.startNewGame = function () {
        this.route.navigate(['/start']);
    };
    GameService.prototype.openHistory = function () {
        this.route.navigate(['/allGames']);
    };
    GameService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"]])
    ], GameService);
    return GameService;
}());



/***/ }),

/***/ "./src/app/shared/services/history.service.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/services/history.service.ts ***!
  \****************************************************/
/*! exports provided: HistoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryService", function() { return HistoryService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HistoryService = /** @class */ (function () {
    function HistoryService(http) {
        this.http = http;
        this.historyAPIControllerUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].historyAPIControllerUrl;
    }
    HistoryService.prototype.getAllGames = function () {
        return this.http.get(this.historyAPIControllerUrl + "GetAllGames");
    };
    HistoryService.prototype.getGameDetails = function (gameId) {
        return this.http.get(this.historyAPIControllerUrl + "GetGameDetails/" + gameId);
    };
    HistoryService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], HistoryService);
    return HistoryService;
}());



/***/ }),

/***/ "./src/app/shared/views/enum-views/card-name-enum.view.ts":
/*!****************************************************************!*\
  !*** ./src/app/shared/views/enum-views/card-name-enum.view.ts ***!
  \****************************************************************/
/*! exports provided: CardNameEnumView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardNameEnumView", function() { return CardNameEnumView; });
var CardNameEnumView;
(function (CardNameEnumView) {
    CardNameEnumView[CardNameEnumView["None"] = 0] = "None";
    CardNameEnumView[CardNameEnumView["Two"] = 1] = "Two";
    CardNameEnumView[CardNameEnumView["Three"] = 2] = "Three";
    CardNameEnumView[CardNameEnumView["Four"] = 3] = "Four";
    CardNameEnumView[CardNameEnumView["Five"] = 4] = "Five";
    CardNameEnumView[CardNameEnumView["Six"] = 5] = "Six";
    CardNameEnumView[CardNameEnumView["Seven"] = 6] = "Seven";
    CardNameEnumView[CardNameEnumView["Eight"] = 7] = "Eight";
    CardNameEnumView[CardNameEnumView["Nine"] = 8] = "Nine";
    CardNameEnumView[CardNameEnumView["Ten"] = 9] = "Ten";
    CardNameEnumView[CardNameEnumView["Jack"] = 10] = "Jack";
    CardNameEnumView[CardNameEnumView["Queen"] = 11] = "Queen";
    CardNameEnumView[CardNameEnumView["King"] = 12] = "King";
    CardNameEnumView[CardNameEnumView["Ace"] = 13] = "Ace";
})(CardNameEnumView || (CardNameEnumView = {}));


/***/ }),

/***/ "./src/app/shared/views/enum-views/card-suit-enum.view.ts":
/*!****************************************************************!*\
  !*** ./src/app/shared/views/enum-views/card-suit-enum.view.ts ***!
  \****************************************************************/
/*! exports provided: CardSuitEnumView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardSuitEnumView", function() { return CardSuitEnumView; });
var CardSuitEnumView;
(function (CardSuitEnumView) {
    CardSuitEnumView[CardSuitEnumView["None"] = 0] = "None";
    CardSuitEnumView[CardSuitEnumView["Diamonds"] = 1] = "Diamonds";
    CardSuitEnumView[CardSuitEnumView["Hearts"] = 2] = "Hearts";
    CardSuitEnumView[CardSuitEnumView["Spades"] = 3] = "Spades";
    CardSuitEnumView[CardSuitEnumView["Clubs"] = 4] = "Clubs";
})(CardSuitEnumView || (CardSuitEnumView = {}));


/***/ }),

/***/ "./src/app/shared/views/enum-views/get-card-enum.view.ts":
/*!***************************************************************!*\
  !*** ./src/app/shared/views/enum-views/get-card-enum.view.ts ***!
  \***************************************************************/
/*! exports provided: GetCardEnum */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetCardEnum", function() { return GetCardEnum; });
var GetCardEnum;
(function (GetCardEnum) {
    GetCardEnum[GetCardEnum["None"] = 0] = "None";
    GetCardEnum[GetCardEnum["Start"] = 1] = "Start";
    GetCardEnum[GetCardEnum["Take"] = 2] = "Take";
})(GetCardEnum || (GetCardEnum = {}));


/***/ }),

/***/ "./src/app/shared/views/enum-views/player-status-enum.view.ts":
/*!********************************************************************!*\
  !*** ./src/app/shared/views/enum-views/player-status-enum.view.ts ***!
  \********************************************************************/
/*! exports provided: PlayerStatusEnumView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerStatusEnumView", function() { return PlayerStatusEnumView; });
var PlayerStatusEnumView;
(function (PlayerStatusEnumView) {
    PlayerStatusEnumView[PlayerStatusEnumView["None"] = 0] = "None";
    PlayerStatusEnumView[PlayerStatusEnumView["InGame"] = 1] = "InGame";
    PlayerStatusEnumView[PlayerStatusEnumView["Winner"] = 2] = "Winner";
    PlayerStatusEnumView[PlayerStatusEnumView["Looser"] = 3] = "Looser";
    PlayerStatusEnumView[PlayerStatusEnumView["Draw"] = 4] = "Draw";
    PlayerStatusEnumView[PlayerStatusEnumView["BlackJack"] = 5] = "BlackJack";
})(PlayerStatusEnumView || (PlayerStatusEnumView = {}));


/***/ }),

/***/ "./src/app/shared/views/enum-views/role-enum.view.ts":
/*!***********************************************************!*\
  !*** ./src/app/shared/views/enum-views/role-enum.view.ts ***!
  \***********************************************************/
/*! exports provided: RoleEnumView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleEnumView", function() { return RoleEnumView; });
var RoleEnumView;
(function (RoleEnumView) {
    RoleEnumView[RoleEnumView["None"] = 0] = "None";
    RoleEnumView[RoleEnumView["Dealer"] = 1] = "Dealer";
    RoleEnumView[RoleEnumView["Bot"] = 2] = "Bot";
    RoleEnumView[RoleEnumView["Human"] = 3] = "Human";
})(RoleEnumView || (RoleEnumView = {}));


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: false,
    gameAPIControllerUrl: 'http://localhost:58816/api/Game/',
    historyAPIControllerUrl: 'http://localhost:58816/api/History/'
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Anuitex\Desktop\bittest\BlackJack\BlackJack.AngularMVC\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map