(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["src-app-game-current-game-current-game-module"],{

/***/ "./src/app/Game/current-game/current-game-component/current-game.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/Game/current-game/current-game-component/current-game.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n"

/***/ }),

/***/ "./src/app/Game/current-game/current-game-component/current-game.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/Game/current-game/current-game-component/current-game.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Game number {{gameId}}</h1>\r\n<table class=\"table1\">\r\n    <tr><th>Name </th><th>Role </th><th>Cards </th><th>Total points </th><th>Status </th></tr>\r\n    <tr *ngFor=\"let player of currentGame.players\">\r\n        <td>{{player.name}}</td>\r\n        <td>{{this.gameService.getTypeRole(player.role)}}</td>\r\n        <td> \r\n            <div *ngFor=\"let card of player.playerCards\">\r\n                {{this.gameService.getTypeCardName(card.name)}} {{this.gameService.getTypeCardSuit(card.suit)}} {{card.points}}\r\n            </div>\r\n            <br />\r\n        </td>\r\n        <td>\r\n            {{player.cardSum}}\r\n        </td>\r\n        <td>\r\n            {{this.gameService.getTypePlayerStatus(player.playerStatus)}}\r\n        </td>\r\n    </tr>\r\n    <tr>\r\n        <td>{{currentGame.dealerPlayer.name}}</td>\r\n        <td>{{this.gameService.getTypeRole(currentGame.dealerPlayer.role)}}</td>\r\n        <td> \r\n            <div *ngFor=\"let card of currentGame.dealerPlayer.playerCards\">\r\n                {{this.gameService.getTypeCardName(card.name)}} {{this.gameService.getTypeCardSuit(card.suit)}} {{card.points}}\r\n            </div>\r\n            <br />\r\n        </td>\r\n        <td>\r\n            {{currentGame.dealerPlayer.cardSum}}\r\n        </td>\r\n        <td>\r\n            -\r\n        </td>\r\n    </tr>\r\n</table>\r\n<br>\r\n<br />\r\n<button kendoButton (click)=\"createNextRound(2)\">Take more</button>\r\n<br />\r\n<br />\r\n<button kendoButton (click)=\"endGame()\">Enough</button>"

/***/ }),

/***/ "./src/app/Game/current-game/current-game-component/current-game.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/Game/current-game/current-game-component/current-game.component.ts ***!
  \************************************************************************************/
/*! exports provided: CurrentGameComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrentGameComponent", function() { return CurrentGameComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/services/game.service */ "./src/app/shared/services/game.service.ts");
/* harmony import */ var src_app_shared_views_enum_views_game_end_enum_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/views/enum-views/game-end-enum.view */ "./src/app/shared/views/enum-views/game-end-enum.view.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CurrentGameComponent = /** @class */ (function () {
    function CurrentGameComponent(gameService, activateRoute, route) {
        this.gameService = gameService;
        this.activateRoute = activateRoute;
        this.route = route;
        this.gameId = this.activateRoute.snapshot.params['id'];
        this.getCardEnum = this.activateRoute.snapshot.params['getCardEnum'];
    }
    CurrentGameComponent.prototype.ngOnInit = function () {
        this.createFirstRound(this.gameId, this.getCardEnum);
    };
    CurrentGameComponent.prototype.createFirstRound = function (gameId, getCardEnum) {
        var _this = this;
        this.gameService.playCurrentGame(gameId, getCardEnum)
            .subscribe(function (currentGame) {
            _this.currentGame = currentGame;
            if (currentGame.checkEndGame == src_app_shared_views_enum_views_game_end_enum_view__WEBPACK_IMPORTED_MODULE_3__["GameEndEnumView"].EndGame) {
                _this.endGame();
            }
        });
    };
    CurrentGameComponent.prototype.createNextRound = function (getCardEnumId) {
        var _this = this;
        this.gameService.playCurrentGame(this.gameId, this.gameService.getTypeCardEnum(getCardEnumId))
            .subscribe(function (nextRound) {
            _this.currentGame = nextRound;
            if (nextRound.checkEndGame == src_app_shared_views_enum_views_game_end_enum_view__WEBPACK_IMPORTED_MODULE_3__["GameEndEnumView"].EndGame) {
                _this.endGame();
            }
        });
    };
    CurrentGameComponent.prototype.endGame = function () {
        this.route.navigate(['/endGame', this.gameId]);
    };
    CurrentGameComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-current-game',
            template: __webpack_require__(/*! ./current-game.component.html */ "./src/app/Game/current-game/current-game-component/current-game.component.html"),
            styles: [__webpack_require__(/*! ./current-game.component.css */ "./src/app/Game/current-game/current-game-component/current-game.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], CurrentGameComponent);
    return CurrentGameComponent;
}());



/***/ }),

/***/ "./src/app/game/current-game/current-game-routing.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/game/current-game/current-game-routing.module.ts ***!
  \******************************************************************/
/*! exports provided: CurrentGameRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrentGameRoutingModule", function() { return CurrentGameRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_Game_current_game_current_game_component_current_game_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Game/current-game/current-game-component/current-game.component */ "./src/app/Game/current-game/current-game-component/current-game.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: src_app_Game_current_game_current_game_component_current_game_component__WEBPACK_IMPORTED_MODULE_2__["CurrentGameComponent"]
    }
];
var CurrentGameRoutingModule = /** @class */ (function () {
    function CurrentGameRoutingModule() {
    }
    CurrentGameRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], CurrentGameRoutingModule);
    return CurrentGameRoutingModule;
}());



/***/ }),

/***/ "./src/app/game/current-game/current-game.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/game/current-game/current-game.module.ts ***!
  \**********************************************************/
/*! exports provided: CurrentGameModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrentGameModule", function() { return CurrentGameModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _current_game_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./current-game-routing.module */ "./src/app/game/current-game/current-game-routing.module.ts");
/* harmony import */ var src_app_Game_current_game_current_game_component_current_game_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Game/current-game/current-game-component/current-game.component */ "./src/app/Game/current-game/current-game-component/current-game.component.ts");
/* harmony import */ var src_app_shared_modules_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/modules/shared.module */ "./src/app/shared/modules/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CurrentGameModule = /** @class */ (function () {
    function CurrentGameModule() {
    }
    CurrentGameModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                src_app_shared_modules_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"],
                _current_game_routing_module__WEBPACK_IMPORTED_MODULE_1__["CurrentGameRoutingModule"],
            ],
            declarations: [src_app_Game_current_game_current_game_component_current_game_component__WEBPACK_IMPORTED_MODULE_2__["CurrentGameComponent"]]
        })
    ], CurrentGameModule);
    return CurrentGameModule;
}());



/***/ }),

/***/ "./src/app/shared/views/enum-views/game-end-enum.view.ts":
/*!***************************************************************!*\
  !*** ./src/app/shared/views/enum-views/game-end-enum.view.ts ***!
  \***************************************************************/
/*! exports provided: GameEndEnumView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameEndEnumView", function() { return GameEndEnumView; });
var GameEndEnumView;
(function (GameEndEnumView) {
    GameEndEnumView[GameEndEnumView["None"] = 0] = "None";
    GameEndEnumView[GameEndEnumView["EndGame"] = 1] = "EndGame";
    GameEndEnumView[GameEndEnumView["ContinueGame"] = 2] = "ContinueGame";
})(GameEndEnumView || (GameEndEnumView = {}));


/***/ })

}]);
//# sourceMappingURL=src-app-game-current-game-current-game-module.js.map