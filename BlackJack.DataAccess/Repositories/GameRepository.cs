﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using BlackJack.Entities;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using BlackJack.DataAccess.Repositories.Interfaces;

namespace BlackJack.DataAccess.Repositories
{
    public class GameRepository : IGameRepository
    {
        private readonly string _connectionString;
        public GameRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<IEnumerable<Game>> GetAll()
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var allGames = await connection.QueryAsync<Game>("SELECT * FROM Games");
                return allGames;
            }
        }
        public async Task<int> Create(Game game)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var gameId = await connection.InsertAsync(game);
                return gameId;
            }        
        }

    }
}
