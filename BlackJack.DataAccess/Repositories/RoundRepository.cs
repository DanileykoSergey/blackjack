﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities;
using Dapper;
using Dapper.Contrib.Extensions;

namespace BlackJack.DataAccess.Repositories
{
    public class RoundRepository : IRoundRepository
    {
        private readonly string _connectionString;

        public RoundRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Create(Round round)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var roundId = await connection.InsertAsync(round);
                return roundId;
            }
        }

        public async Task<IEnumerable<Round>> GetAllRoundsInTheGame(int gameId)
        {
            string sql = @"SELECT * 
                            FROM Rounds AS A
                            JOIN Players AS B ON A.PlayerId = B.Id
                            JOIN Card AS C ON A.CardId = C.Id
                            WHERE GameId = @GameId";


            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var rounds = await connection.QueryAsync<Round, Player, Card, Round>(
                        sql, 
                        (round, player, card) =>
                        {
                            round.Player = player;
                            round.Card = card;
                            return round;
                        },
                        new { gameId }
                        );
                return rounds;
            }
        }
    }
}