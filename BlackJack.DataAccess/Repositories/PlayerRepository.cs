﻿using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly string _connectionString;
        public PlayerRepository(string connectionString)
        {
            _connectionString = connectionString;
        }


        public async Task<Player> GetPlayer(string name)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var player = (await connection.QueryAsync<Player>("SELECT * FROM Players WHERE Name = @Name", new { name })).FirstOrDefault();
                return player;
            }
        }

        public async Task<IEnumerable<Player>> GetAllHumanPlayers()
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var humanPlayers = await connection.QueryAsync<Player>("SELECT * FROM Players where Role=3");
                return humanPlayers;
            }
        }

     
        public async Task<IEnumerable<Player>> GetBotPlayers(int count)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {     
                var botPlayers = await connection.QueryAsync<Player>("SELECT * FROM Players WHERE Role=2 AND Id BETWEEN 1 AND @count", new { count });
                return botPlayers;
            }
        }

        public async Task<Player> GetDealerPlayer()
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var dealerPlayer = (await connection.QueryAsync<Player>("SELECT * FROM Players where Role=1")).FirstOrDefault();
                return dealerPlayer;
            }
        }

        public async Task<int> Create(Player player)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var playerId = await connection.InsertAsync(player);
                return playerId;
            }
        }
    }
}