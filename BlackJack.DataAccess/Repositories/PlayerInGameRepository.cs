﻿using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories
{
    public class PlayersInGameRepository : IPlayerInGameRepository
    {
        private readonly string _connectionString;
        public PlayersInGameRepository(string connectionString)
        {
            _connectionString = connectionString;
        }


        public async Task Create(List<PlayerInGame> playersOnTheGame)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                await connection.InsertAsync(playersOnTheGame);
            }
        }


        public async Task UpdatePlayersStatus(List<PlayerInGame> playersOnTheGame)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.UpdateAsync(playersOnTheGame);
            }
        }

        public async Task<PlayerInGame> GetHumanPlayerOnTheGame(int gameId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var humanOnTheGame = (await connection.QueryAsync<PlayerInGame>(@"SELECT * 
                                                                    FROM PlayersInGames AS A 
                                                                    JOIN Players AS B ON A.PlayerId = B.Id 
                                                                    WHERE GameId = @GameId And B.Role = 3 ", new { gameId })).FirstOrDefault();
                return humanOnTheGame;
            }
        }

        public async Task<PlayerInGame> GetDealerPlayerOnTheGame(int gameId)
        {
            string sql = @"SELECT * 
                             FROM PlayersInGames AS A 
                             JOIN Players AS B ON A.PlayerId = B.Id 
                             WHERE GameId = @GameId And B.Role = 1";
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var dealerOnTheGame = (await connection.QueryAsync<PlayerInGame, Player, PlayerInGame>(
                    sql,
                    (playersGames, player) =>
                    {
                        playersGames.Player = player;
                        return playersGames;
                    },
                    new { gameId }
                    )).FirstOrDefault();
                return dealerOnTheGame;
            }

        }

        public async Task<IEnumerable<PlayerInGame>> GetAllPlayersOnTheGame(int gameId)
        {
            string sql = @"SELECT * 
                            FROM PlayersInGames AS A
                            JOIN Players AS B ON A.PlayerId = B.Id
                            JOIN Games AS C ON A.GameId = C.Id
                            WHERE GameId = @GameId";
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var allPlayersOnTheGame = await connection.QueryAsync<PlayerInGame, Player, Game, PlayerInGame>(
                        sql,
                        (playersGames, player, game) =>
                        {
                            playersGames.Player = player;
                            playersGames.Game = game;
                            return playersGames;
                        },
                        new { gameId }
                        );
                return allPlayersOnTheGame;
            }
        }
    }
}