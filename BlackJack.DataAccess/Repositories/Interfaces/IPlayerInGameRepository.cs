﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlackJack.Entities;


namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface IPlayerInGameRepository
    {
        Task Create(List<PlayerInGame> playerGames);
        Task<PlayerInGame> GetHumanPlayerOnTheGame(int gameId);
        Task<PlayerInGame> GetDealerPlayerOnTheGame(int gameId);
        Task UpdatePlayersStatus(List<PlayerInGame> playersOnTheGame);
        Task<IEnumerable<PlayerInGame>> GetAllPlayersOnTheGame(int gameId);
    }
}
