﻿using BlackJack.Entities;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface ICardRepository
    {
        Task<Card> GetRandom();
    }
}
