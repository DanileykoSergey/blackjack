﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlackJack.Entities;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface IPlayerRepository
    {
        Task<int> Create(Player player);
        Task<Player> GetDealerPlayer();
        Task<Player> GetPlayer(string name);
        Task<IEnumerable<Player>> GetAllHumanPlayers();
        Task<IEnumerable<Player>> GetBotPlayers(int count);
    }
}
