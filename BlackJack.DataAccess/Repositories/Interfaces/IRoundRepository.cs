﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlackJack.Entities;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface IRoundRepository
    {
        Task<int> Create(Round round);
        Task<IEnumerable<Round>> GetAllRoundsInTheGame(int gameId);
    }
}
