﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities;
using Dapper;

namespace BlackJack.DataAccess.Repositories
{
    public class CardRepository : ICardRepository
    {
        private readonly string _connectionString;
        public CardRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        public async Task<Card> GetRandom()
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var card = (await connection.QueryAsync<Card>("SELECT TOP 1 * FROM Card ORDER BY NEWID()")).FirstOrDefault();
                return card;
            }
        }
    }
}