﻿using Autofac;
using BlackJack.DataAccess.Repositories;
using BlackJack.DataAccess.Repositories.Interfaces;

namespace BlackJack.DataAccess.Configs
{
    public class AutofacRepositoriesConfig
    {
        public static void ConfigureContainer(ContainerBuilder builder, string connectionString)
        {
            builder.RegisterType<PlayerRepository>().As<IPlayerRepository>().WithParameter("connectionString", connectionString).InstancePerRequest();
            builder.RegisterType<RoundRepository>().As<IRoundRepository>().WithParameter("connectionString", connectionString).InstancePerRequest();
            builder.RegisterType<GameRepository>().As<IGameRepository>().WithParameter("connectionString", connectionString).InstancePerRequest();
            builder.RegisterType<PlayersInGameRepository>().As<IPlayerInGameRepository>().WithParameter("connectionString", connectionString).InstancePerRequest();
            builder.RegisterType<CardRepository>().As<ICardRepository>().WithParameter("connectionString", connectionString).InstancePerRequest();                      
        }
    }
}
