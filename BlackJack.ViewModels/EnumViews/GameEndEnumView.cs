﻿namespace BlackJack.ViewModels.EnumViews
{
    public enum GameEndEnumView
    {
        None = 0,
        EndGame = 1,
        ContinueGame = 2   
    }
}
