﻿using BlackJack.ViewModels.EnumViews;
using System.Collections.Generic;

namespace BlackJack.ViewModels.GameViews
{
    public class GetEndGameGameView
    {
        public List<PlayerGetEndGameGameViewItem> Players = new List<PlayerGetEndGameGameViewItem>();
        public PlayerGetEndGameGameViewItem DealerPlayer = new PlayerGetEndGameGameViewItem();
        public GameEndEnumView CheckEndGame;
        public long GameId;
    }
    public class PlayerGetEndGameGameViewItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int CardSum { get; set; }
        public PlayerStatusEnumView PlayerStatus;
        public RoleEnumView Role { get; set; }
        public List<CardGetEndGameGameViewItem> PlayerCards = new List<CardGetEndGameGameViewItem>();
    }
    public class CardGetEndGameGameViewItem
    {
        public long Id { get; set; }
        public CardNameEnumView Name { get; set; }
        public int Points { get; set; }
        public CardSuitEnumView Suit { get; set; }
    }
}
