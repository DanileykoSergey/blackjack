﻿using System.Collections.Generic;

namespace BlackJack.ViewModels.GameViews
{
    public class GetStartGameView
    {
        public List<PlayerGetStartGameViewItem> HumanPlayers = new List<PlayerGetStartGameViewItem>();    
    }
    public class PlayerGetStartGameViewItem
    {
        public long Id { get; set; }
        public string Name { get; set; }       
    }
}
