﻿using BlackJack.ViewModels.EnumViews;
using System.Collections.Generic;

namespace BlackJack.ViewModels.GameViews
{
    public class PlayCurrentGameGameView
    {
        public List<PlayerPlayCurrentGameGameViewItem> Players = new List<PlayerPlayCurrentGameGameViewItem>();
        public PlayerPlayCurrentGameGameViewItem DealerPlayer = new PlayerPlayCurrentGameGameViewItem();
        public GameEndEnumView CheckEndGame;
        public long GameId;
    }
    public class PlayerPlayCurrentGameGameViewItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int CardSum { get; set; }
        public PlayerStatusEnumView PlayerStatus;
        public RoleEnumView Role { get; set; }
        public List<CardPlayCurrentGameGameViewItem> PlayerCards = new List<CardPlayCurrentGameGameViewItem>();
    }
    public class CardPlayCurrentGameGameViewItem
    {
        public long Id { get; set; }
        public CardNameEnumView Name { get; set; }
        public int Points { get; set; }
        public CardSuitEnumView Suit { get; set; }
    }
}
