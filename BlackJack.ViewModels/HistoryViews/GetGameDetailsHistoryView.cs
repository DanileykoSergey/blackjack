﻿using BlackJack.ViewModels.EnumViews;
using System.Collections.Generic;

namespace BlackJack.ViewModels.HistoryViews
{
    public class GetGameDetailsHistoryView
    {
        public List<PlayerGetGameDetailsHistoryViewItem> Players = new List<PlayerGetGameDetailsHistoryViewItem>();
        public PlayerGetGameDetailsHistoryViewItem DealerPlayer = new PlayerGetGameDetailsHistoryViewItem();
        public long GameId;
    }
    public class PlayerGetGameDetailsHistoryViewItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int CardSum { get; set; }
        public PlayerStatusEnumView PlayerStatus;
        public RoleEnumView Role { get; set; }
        public List<CardGetGameDetailsHistoryViewItem> PlayerCards = new List<CardGetGameDetailsHistoryViewItem>();
    }
    public class CardGetGameDetailsHistoryViewItem
    {
        public long Id { get; set; }
        public CardNameEnumView Name { get; set; }
        public int Points { get; set; }
        public CardSuitEnumView Suit { get; set; }
    }
}
