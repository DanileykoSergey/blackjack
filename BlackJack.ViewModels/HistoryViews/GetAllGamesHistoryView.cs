﻿using System;
using System.Collections.Generic;

namespace BlackJack.ViewModels.HistoryViews
{
    public class GetAllGamesHistoryView
    {    
       public List<GetAllGamesHistoryViewItem> Games = new List<GetAllGamesHistoryViewItem>();
    }
    public class GetAllGamesHistoryViewItem
    {
        public long Id { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
