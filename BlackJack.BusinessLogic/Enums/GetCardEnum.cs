﻿namespace BlackJack.BusinessLogic.Enums
{
    public enum GetCardEnum
    {
        None = 0,
        Start = 1,
        Take = 2
    }
}
