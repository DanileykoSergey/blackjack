﻿
namespace BlackJack.BusinessLogic.Configs
{
    public class Config
    {
        public const int DoubleAcePointReduce = 10;
        public const int DoubleAcePoint = 1;
        public const int BlackJack = 21;
        public const int DealerMinTotalPoint = 17;
        public const int ZeroingOutCardSum = 0;
    }
}
