﻿using Autofac;
using BlackJack.BusinessLogic.Services;
using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.DataAccess.Configs;

namespace BlackJack.BusinessLogic.Configs
{
    public class AutofacServicesConfig
    {
        public static void ConfigureContainer(ContainerBuilder builder, string connectionString)
        {
            builder.RegisterType<HistoryService>().As<IHistoryService>().InstancePerRequest();
            builder.RegisterType<GameService>().As<IGameService>().InstancePerRequest();
            AutofacRepositoriesConfig.ConfigureContainer(builder, connectionString);
        }
    }
}
