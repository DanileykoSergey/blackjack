﻿using System;

namespace BlackJack.BusinessLogic.Exceptions
{
    public class AppValidationException : Exception
    {
        public AppValidationException(string message) : base(message)
        {
        }
    }
}
