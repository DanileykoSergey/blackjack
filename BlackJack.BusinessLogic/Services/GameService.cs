﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using MoreLinq;
using BlackJack.Entities;
using BlackJack.Entities.Enums;
using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.ViewModels.GameViews;
using BlackJack.BusinessLogic.Exceptions;
using BlackJack.BusinessLogic.Enums;
using BlackJack.ViewModels.EnumViews;
using BlackJack.BusinessLogic.Configs;

namespace BlackJack.BusinessLogic.Services
{
    public class GameService : BaseService, IGameService
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly IRoundRepository _roundRepository;
        private readonly IPlayerInGameRepository _playerInGameRepository;
        private readonly IGameRepository _gameRepository;
        private readonly ICardRepository _cardRepository;
        public GameService(
            IPlayerRepository playersRepository,
            IRoundRepository roundRepository,
            IPlayerInGameRepository playerInGameRepository,
            IGameRepository gameRepository,
            ICardRepository cardRepository
            )
        {
            _playerRepository = playersRepository;
            _roundRepository = roundRepository;
            _playerInGameRepository = playerInGameRepository;
            _gameRepository = gameRepository;
            _cardRepository = cardRepository;
        }

        public async Task<GetStartGameView> GetStart()
        {
            IEnumerable<Player> humanPlayers = await _playerRepository.GetAllHumanPlayers();
            var allHumanPlayers = new GetStartGameView();
            allHumanPlayers.HumanPlayers = humanPlayers.Select(x => new PlayerGetStartGameViewItem { Id = x.Id, Name = x.Name }).ToList();
            return allHumanPlayers;
        }

        public async Task<int> CreateGame(string currentPlayer, int botsCount)
        {
            if (currentPlayer == string.Empty)
            {
                throw new AppValidationException("Error: empty name");
            }
            var game = new Game();
            int gameId = await _gameRepository.Create(game);
            Player userPlayer = await CreateOrSelectNewPlayer(currentPlayer);
            Player dealerPlayer = await _playerRepository.GetDealerPlayer();
            List<Player> botPlayers = (await _playerRepository.GetBotPlayers(botsCount)).ToList();
            if (userPlayer == null || dealerPlayer == null || botPlayers == null)
            {
                throw new AppValidationException("Something went wrong");
            }
            List<Player> playersOnTheGame = botPlayers;
            playersOnTheGame.Add(userPlayer);
            playersOnTheGame.Add(dealerPlayer);
            var playersGame = playersOnTheGame.Select(x => new PlayerInGame
            {
                PlayerId = x.Id,
                GameId = gameId,
                PlayerStatus = PlayerStatus.InGame,
            }).ToList();
            await _playerInGameRepository.Create(playersGame);
            return gameId;
        }

        public async Task<PlayCurrentGameGameView> PlayCurrentGame(int gameId, GetCardEnum getCardEnum)
        {
            if (getCardEnum == GetCardEnum.Start)
            {
                return await CreateFirstRoundForAllPlayers(gameId);
            }
            return await ContinueGameForPlayers(gameId);
        }

        public async Task<GetEndGameGameView> GetEndGame(int gameId)
        {
            PlayerInGame dealerPlayer = await _playerInGameRepository.GetDealerPlayerOnTheGame(gameId);
            List<Round> rounds = (await _roundRepository.GetAllRoundsInTheGame(gameId)).ToList();
            int dealerCardSum = await CalculationPlayerCardSum(dealerPlayer.Player.Id, gameId, rounds);
            var endGame = new GetEndGameGameView();
            if (!await CheckAllPlayerLoose(gameId))
            {
                return endGame = await GetInformationForEndGame(gameId);
            }
            if (dealerCardSum >= Config.DealerMinTotalPoint)
            {
                return endGame = await GetInformationForEndGame(gameId);
            }
            if (dealerCardSum < Config.DealerMinTotalPoint)
            {
                await CreateNextRoundForPlayer(dealerPlayer.Player.Id, gameId);
                endGame.DealerPlayer.CardSum = await CalculationPlayerCardSum(dealerPlayer.Player.Id, gameId, rounds);
            }
            if (!CheckBust(endGame.DealerPlayer.CardSum))
            {
                await GetEndGame(gameId);
            }
            return endGame = await GetInformationForEndGame(gameId);
        }

        private async Task<Player> CreateOrSelectNewPlayer(string name)
        {
            Player player = await _playerRepository.GetPlayer(name);
            if (player != null)
            {
                return player;
            }
            player = new Player();
            player.Name = name;
            player.Role = Role.Human;
            player.Id = await _playerRepository.Create(player);
            return player;
        }

        private async Task<PlayCurrentGameGameView> CreateFirstRoundForAllPlayers(int gameId)
        {
            List<PlayerInGame> playersOnTheGame = (await _playerInGameRepository.GetAllPlayersOnTheGame(gameId)).ToList();
            foreach (PlayerInGame player in playersOnTheGame)
            {
                await CreateFirstRoundsForPlayer(player.PlayerId, player.GameId);
            }
            List<PlayerPlayCurrentGameGameViewItem> playersOnTheGameViewItem = await GetInformationAboutPlayers(gameId);
            PlayerPlayCurrentGameGameViewItem dealerPlayerViewItem = playersOnTheGameViewItem.Where(x => x.Role == (RoleEnumView)Role.Dealer).First();
            playersOnTheGameViewItem.Remove(dealerPlayerViewItem);
            var currentGame = new PlayCurrentGameGameView();
            currentGame.DealerPlayer = dealerPlayerViewItem;
            currentGame.Players = playersOnTheGameViewItem;
            currentGame.GameId = gameId;
            if ((dealerPlayerViewItem.PlayerStatus == PlayerStatusEnumView.BlackJack) || (playersOnTheGameViewItem.Where(x => x.Role == (RoleEnumView)Role.Human).First()).PlayerStatus == (PlayerStatusEnumView)PlayerStatus.BlackJack)
            {
                currentGame.CheckEndGame = GameEndEnumView.EndGame;
            }
            return currentGame;
        }

        private async Task CreateFirstRoundsForPlayer(long playerOnTheGameId, long gameId)
        {
            await CreateNextRoundForPlayer(playerOnTheGameId, gameId);
            await CreateNextRoundForPlayer(playerOnTheGameId, gameId);
        }

        private async Task CreateNextRoundForPlayer(long playerOnTheGameId, long gameId)
        {
            var round = new Round();
            Card card = await _cardRepository.GetRandom();
            round.PlayerId = playerOnTheGameId;
            round.GameId = gameId;
            round.CardId = card.Id;
            await _roundRepository.Create(round);
        }

        private async Task<PlayCurrentGameGameView> ContinueGameForPlayers(int gameId)
        {
            List<PlayerInGame> playersOnTheGame = (await _playerInGameRepository.GetAllPlayersOnTheGame(gameId)).ToList();
            PlayerInGame dealerPlayer = playersOnTheGame.Where(x => x.Player.Role == Role.Dealer).First();
            foreach (PlayerInGame player in playersOnTheGame.Where(x => x.Player.Role != Role.Dealer).ToList())
            {
                if (player.PlayerStatus == PlayerStatus.InGame)
                {
                    await CreateNextRoundForPlayer(player.PlayerId, gameId);
                }
            }
            List<PlayerPlayCurrentGameGameViewItem> playersOnTheCurrentGame = await GetInformationAboutPlayers(gameId);
            PlayerPlayCurrentGameGameViewItem dealerPlayerOnTheCurrentGame = playersOnTheCurrentGame.Where(x => x.Role == (RoleEnumView)Role.Dealer).First();
            playersOnTheCurrentGame.Remove(dealerPlayerOnTheCurrentGame);
            var continueGame = new PlayCurrentGameGameView();
            continueGame.DealerPlayer = dealerPlayerOnTheCurrentGame;
            continueGame.Players = playersOnTheCurrentGame;
            continueGame.GameId = gameId;
            var checkResultHumanPlayer = await CheckResultHumanPlayer(gameId);
            if (!await CheckAllPlayerLoose(gameId))
            {
                continueGame.CheckEndGame = GameEndEnumView.EndGame;
                return continueGame;
            }
            if (!checkResultHumanPlayer)
            {
                continueGame.CheckEndGame = GameEndEnumView.EndGame;
                return continueGame;
            }
            continueGame.CheckEndGame = GameEndEnumView.ContinueGame;
            return continueGame;
        }

        private async Task<List<PlayerPlayCurrentGameGameViewItem>> GetInformationAboutPlayers(int gameId)
        {
            List<Round> rounds = (await _roundRepository.GetAllRoundsInTheGame(gameId)).ToList();
            var playersOnTheGame = rounds.Select(x => new PlayerPlayCurrentGameGameViewItem()
            {
                Id = x.Player.Id,
                Name = x.Player.Name,
                Role = (RoleEnumView)x.Player.Role,
                PlayerCards = rounds.Where(y => y.Player.Id == x.Player.Id)
                    .Select(c => new CardPlayCurrentGameGameViewItem
                    {
                        Id = c.Card.Id,
                        Name = (CardNameEnumView)c.Card.Name,
                        Points = c.Card.Points,
                        Suit = (CardSuitEnumView)c.Card.Suit
                    })
                    .ToList()
            }).DistinctBy(x => x.Id).ToList();
            List<PlayerInGame> playersGame = (await _playerInGameRepository.GetAllPlayersOnTheGame(gameId)).ToList();
            foreach (PlayerInGame player in playersGame)
            {
                int playerCardSum = await CalculationPlayerCardSum(player.PlayerId, gameId, rounds);
                int dealerCardSum = await CalculationPlayerCardSum(playersGame.Where(x => x.Player.Role == Role.Dealer).First().Id, gameId, rounds);

                if (CheckBlackJack(playerCardSum))
                {
                    player.PlayerStatus = PlayerStatus.BlackJack;
                }
                if (CheckBust(playerCardSum))
                {
                    player.PlayerStatus = PlayerStatus.Looser;
                }
                if ((dealerCardSum == Config.BlackJack) && (player.PlayerStatus != PlayerStatus.BlackJack))
                {
                    player.PlayerStatus = PlayerStatus.Looser;
                }
                if ((dealerCardSum == Config.BlackJack) && (player.PlayerStatus == PlayerStatus.BlackJack))
                {
                    player.PlayerStatus = PlayerStatus.Draw;
                }

            }
            await _playerInGameRepository.UpdatePlayersStatus(playersGame);
            for (int i = 0; i < playersOnTheGame.Count; i++)
            {
                playersOnTheGame[i].CardSum = await CalculationPlayerCardSum(playersOnTheGame[i].Id, gameId, rounds);
                playersOnTheGame[i].PlayerStatus = (PlayerStatusEnumView)playersGame[i].PlayerStatus;
            }
            return playersOnTheGame;
        }

        private async Task<bool> CheckAllPlayerLoose(int gameId)
        {
            List<PlayerInGame> playersOnTheGame = (await _playerInGameRepository.GetAllPlayersOnTheGame(gameId))
                .Where(result => result.Player.Role != Role.Dealer).ToList();
            bool anyLooser = playersOnTheGame.Any(r => r.PlayerStatus == PlayerStatus.InGame || r.PlayerStatus == PlayerStatus.BlackJack || r.PlayerStatus == PlayerStatus.Winner || r.PlayerStatus == PlayerStatus.Draw);
            return anyLooser;
        }

        private async Task<bool> CheckResultHumanPlayer(int gameId)
        {
            PlayerInGame humanPlayerOnTheGame = await _playerInGameRepository.GetHumanPlayerOnTheGame(gameId);
            return humanPlayerOnTheGame.PlayerStatus == PlayerStatus.InGame;
        }

        private bool CheckBlackJack(int cardsum)
        {
            return cardsum == Config.BlackJack;
        }

        private bool CheckBust(int cardsum)
        {
            return cardsum > Config.BlackJack;
        }

        private async Task<GetEndGameGameView> GetInformationForEndGame(int gameId)
        {
            List<Round> rounds = (await _roundRepository.GetAllRoundsInTheGame(gameId)).ToList();
            List<PlayerInGame> playersOnTheGame = (await _playerInGameRepository.GetAllPlayersOnTheGame(gameId)).ToList();
            PlayerInGame dealerPlayer = playersOnTheGame.Where(x => x.Player.Role == Role.Dealer).First();
            int dealerCardSum = await CalculationPlayerCardSum(dealerPlayer.PlayerId, gameId, rounds);
            foreach (PlayerInGame player in playersOnTheGame)
            {
                int playerCardSum = await CalculationPlayerCardSum(player.PlayerId, gameId, rounds);
                if (CheckBust(dealerCardSum) && (!CheckBust(playerCardSum)))
                {
                    player.PlayerStatus = PlayerStatus.Winner;
                }
                if (!CheckBust(dealerCardSum) && dealerCardSum > playerCardSum)
                {
                    player.PlayerStatus = PlayerStatus.Looser;
                }
                if (dealerCardSum == playerCardSum && (!CheckBust(playerCardSum)))
                {
                    player.PlayerStatus = PlayerStatus.Draw;
                }
                if (!CheckBust(playerCardSum) && dealerCardSum < playerCardSum)
                {
                    player.PlayerStatus = PlayerStatus.Winner;
                }
            }
            await _playerInGameRepository.UpdatePlayersStatus(playersOnTheGame);
            var playersOnTheGameEndGame = rounds.Select(x => new PlayerGetEndGameGameViewItem()
            {
                Id = x.Player.Id,
                Name = x.Player.Name,
                Role = (RoleEnumView)x.Player.Role,
                PlayerCards = rounds.Where(y => y.Player.Id == x.Player.Id)
                    .Select(c => new CardGetEndGameGameViewItem
                    {
                        Id = c.Card.Id,
                        Name = (CardNameEnumView)c.Card.Name,
                        Points = c.Card.Points,
                        Suit = (CardSuitEnumView)c.Card.Suit
                    }).ToList()
            }).DistinctBy(x => x.Id).ToList();

            for (int i = 0; i < playersOnTheGameEndGame.Count; i++)
            {
                playersOnTheGameEndGame[i].CardSum = await CalculationPlayerCardSum(playersOnTheGameEndGame[i].Id, gameId, rounds);
                playersOnTheGameEndGame[i].PlayerStatus = (PlayerStatusEnumView)playersOnTheGame[i].PlayerStatus;
            }
            PlayerGetEndGameGameViewItem dealerOnTheEndGame = playersOnTheGameEndGame.Where(x => x.Role == (RoleEnumView)Role.Dealer).First();
            playersOnTheGameEndGame.Remove(dealerOnTheEndGame);
            var endGame = new GetEndGameGameView();
            endGame.DealerPlayer = dealerOnTheEndGame;
            endGame.Players = playersOnTheGameEndGame;
            endGame.GameId = gameId;
            return endGame;
        }
    }
}