﻿using BlackJack.BusinessLogic.Enums;
using BlackJack.ViewModels.GameViews;
using System.Threading.Tasks;

namespace BlackJack.BusinessLogic.Services.Interfaces
{
    public interface IGameService
    {
        Task<GetStartGameView> GetStart();
        Task<int> CreateGame(string currentPlayer, int botsCount);
        Task<PlayCurrentGameGameView> PlayCurrentGame(int gameId, GetCardEnum getCardEnum);
        Task<GetEndGameGameView> GetEndGame(int gameId);
    }
}
