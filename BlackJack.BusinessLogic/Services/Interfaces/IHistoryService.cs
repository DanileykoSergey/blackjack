﻿using BlackJack.ViewModels.HistoryViews;
using System.Threading.Tasks;

namespace BlackJack.BusinessLogic.Services.Interfaces
{
    public interface IHistoryService
    {
        Task<GetAllGamesHistoryView> GetAllGames();
        Task<GetGameDetailsHistoryView> GetGameDetails(int gameId);
    }
}
