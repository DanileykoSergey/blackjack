﻿using BlackJack.Entities;
using BlackJack.ViewModels.HistoryViews;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoreLinq;
using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.ViewModels.EnumViews;
using BlackJack.Entities.Enums;

namespace BlackJack.BusinessLogic.Services
{
    public class HistoryService : BaseService, IHistoryService
    {
        private readonly IRoundRepository _roundRepository;
        private readonly IPlayerInGameRepository _playerInGameRepository;
        private readonly IGameRepository _gameRepository;
        public HistoryService(
            IRoundRepository roundRepository,
            IPlayerInGameRepository playerInGameRepository,
            IGameRepository gameRepository
            ) 
        {
            _roundRepository = roundRepository;
            _playerInGameRepository = playerInGameRepository;
            _gameRepository = gameRepository;
        }


        public async Task<GetAllGamesHistoryView> GetAllGames()
        {
            var historyViewModel = new GetAllGamesHistoryView();
            historyViewModel.Games = ((await _gameRepository.GetAll()).ToList()).Select(x => new GetAllGamesHistoryViewItem()
            {
                Id = x.Id,
                CreationDate = x.CreationDate
            }
            ).ToList();
            return historyViewModel;
        }

        public async Task<GetGameDetailsHistoryView> GetGameDetails(int gameId)
        {
            List<Round> rounds = (await _roundRepository.GetAllRoundsInTheGame(gameId)).ToList();
            var playersOnTheGame = rounds.Select(x => new PlayerGetGameDetailsHistoryViewItem()
            {
                Id = x.Player.Id,
                Name = x.Player.Name,
                Role = (RoleEnumView)x.Player.Role,
                PlayerCards = rounds
                .Where(y => y.Player.Id == x.Player.Id)
                .Select(c => new CardGetGameDetailsHistoryViewItem
                {
                    Id = c.Card.Id,
                    Name = (CardNameEnumView)c.Card.Name,
                    Points = c.Card.Points,
                    Suit = (CardSuitEnumView)c.Card.Suit
                }).ToList()
            }).DistinctBy(x => x.Id).ToList();
            List<PlayerInGame> playersGame = (await _playerInGameRepository.GetAllPlayersOnTheGame(gameId)).ToList();
            for (int i = 0; i < playersOnTheGame.Count; i++)
            {
                playersOnTheGame[i].CardSum = await CalculationPlayerCardSum(playersOnTheGame[i].Id, gameId, rounds);
                playersOnTheGame[i].PlayerStatus = (PlayerStatusEnumView)playersGame[i].PlayerStatus;
            }
            var dealerPlayer = playersOnTheGame.Where(x => x.Role == (RoleEnumView)Role.Dealer).First();
            playersOnTheGame.Remove(dealerPlayer);
            var detailsGameViewModel = new GetGameDetailsHistoryView();
            detailsGameViewModel.Players = playersOnTheGame;
            detailsGameViewModel.DealerPlayer = dealerPlayer;
            detailsGameViewModel.GameId = gameId;
            return detailsGameViewModel;
        }
    }
}