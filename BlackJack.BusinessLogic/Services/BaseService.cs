﻿using BlackJack.BusinessLogic.Configs;
using BlackJack.Entities;
using BlackJack.Entities.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.BusinessLogic.Services
{
    public class BaseService
    {
        public async Task<int> CalculationPlayerCardSum(long playerOnTheGameId, long gameId, List<Round> allRoundsInTheGame)
        {
            int cardSum = Config.ZeroingOutCardSum;
            var cardsForPlayer = allRoundsInTheGame
            .Where(round => round.PlayerId == playerOnTheGameId)
            .Select(round => round.Card)
            .ToList();
            foreach (var card in cardsForPlayer)
            {
                cardSum += card.Points;
                if (cardSum > Config.BlackJack && card.Name == CardName.Ace)
                {
                    card.Points = Config.DoubleAcePoint;
                    cardSum -= Config.DoubleAcePointReduce;
                }
            }
            return cardSum;
        }
    }
}
