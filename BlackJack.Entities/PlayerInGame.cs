﻿using BlackJack.Entities.Enums;
using Dapper.Contrib.Extensions;

namespace BlackJack.Entities
{
    [Table("PlayersInGames")]
    public class PlayerInGame : BaseEntity
    {
        public long PlayerId { get; set; }
        [Write(false)]
        public Player Player { get; set; }
        public long GameId { get; set; }
        [Write(false)]
        public Game Game { get; set; }       
        public PlayerStatus PlayerStatus { get; set; }
    }
}
