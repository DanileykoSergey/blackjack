﻿namespace BlackJack.Entities.Enums
{
    public enum PlayerStatus
    {
        None = 0,
        InGame = 1,
        Winner = 2,
        Looser = 3,
        Draw = 4,
        BlackJack = 5
    }
}
