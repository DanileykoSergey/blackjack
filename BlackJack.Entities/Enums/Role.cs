﻿namespace BlackJack.Entities.Enums
{
    public enum Role
    {
        None = 0,
        Dealer = 1,
        Bot = 2,
        Human = 3
    }
}
