﻿using Dapper.Contrib.Extensions;

namespace BlackJack.Entities
{
    public class Round : BaseEntity
    {  
        public long GameId { get; set; }
        [Write(false)]
        public Game Game { get; set; }
        public long PlayerId { get; set; }
        [Write(false)]
        public Player Player { get; set; }       
        public long CardId { get; set; }
        [Write(false)]
        public Card Card { get; set; }
    }
}
