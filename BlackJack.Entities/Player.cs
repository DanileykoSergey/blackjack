﻿using BlackJack.Entities.Enums;

namespace BlackJack.Entities
{
    public class Player : BaseEntity
    {
        public Role Role { get; set; }
        public string Name { get; set; }
    }
}
