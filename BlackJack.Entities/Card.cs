﻿using BlackJack.Entities.Enums;

namespace BlackJack.Entities
{
    public class Card : BaseEntity
    {
        public CardName Name { get; set; }
        public int Points { get; set; }
        public CardSuit Suit { get; set; }
    }
}
