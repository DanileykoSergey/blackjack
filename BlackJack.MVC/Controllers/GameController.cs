﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using BlackJack.BusinessLogic.Enums;
using BlackJack.BusinessLogic.Exceptions;
using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.ViewModels.EnumViews;
using BlackJack.ViewModels.SharedViews;

namespace BlackJack.MVC.Controllers
{
    public class GameController : Controller
    {
        private readonly IGameService _gameService;
        public GameController(IGameService gameService)
        {
            _gameService = gameService;
        }

        public async Task<ActionResult> GetStart()
        {
            var allHumanPlayer = await _gameService.GetStart();
            return View("Start",allHumanPlayer);
        }


        public async Task<ActionResult> PlayCurrentGame(int id, GetCardEnum getCardEnum)
        {
            ModelState.Clear();
            try
            {
                var startGame = await _gameService.PlayCurrentGame(id, getCardEnum);
                if (getCardEnum == GetCardEnum.Start && !(startGame.CheckEndGame == GameEndEnumView.EndGame))
                {
                    return View("CurrentGame",startGame);
                }
                if (startGame.CheckEndGame == GameEndEnumView.EndGame && getCardEnum == GetCardEnum.Start)
                {
                    return RedirectToAction("GetEndGame", "Game", new { id });
                }
                if (getCardEnum == GetCardEnum.Take && (startGame.CheckEndGame == GameEndEnumView.ContinueGame))
                {
                    return View("_CreateNextRoundGameView", startGame);
                }
                return Json(new
                {
                    url = Url.Action("GetEndGame", "Game", new { id }),
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ErrorView errorGameView = new ErrorView();
                errorGameView.Error = e.Message;
                return View("_Error", errorGameView);
            }

        }

        [HttpPost]
        public async Task<ActionResult> CreateGame(string currentPlayer, int botsCount)
        {
            try
            {
                var startGame = await _gameService.CreateGame(currentPlayer, botsCount);
                return RedirectToAction("PlayCurrentGame", "Game", new { id = startGame, GetCardEnum = GetCardEnum.Start });
            }
            catch (AppValidationException e)
            {
                ErrorView errorGameView = new ErrorView();
                errorGameView.Error = e.Message;
                return View("_Error", errorGameView);
            }
            catch (Exception e)
            {
                ErrorView errorGameView = new ErrorView();
                errorGameView.Error = e.Message;
                return View("_Error", errorGameView);
            }
        }

        public async Task<ActionResult> GetEndGame(int id)
        {
            try
            {
                var endGame = await _gameService.GetEndGame(id);
                return View("EndGame",endGame);
            }
            catch (Exception e)
            {
                ErrorView errorGameView = new ErrorView();
                errorGameView.Error = e.Message;
                return View("_Error", errorGameView);
            }
        }
    }
}