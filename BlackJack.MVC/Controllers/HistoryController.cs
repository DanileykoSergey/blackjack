﻿using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.ViewModels.SharedViews;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BlackJack.MVC.Controllers
{
    public class HistoryController : Controller
    {
        private readonly IHistoryService _historyService;
        public HistoryController(IHistoryService historyService)
        {
            _historyService = historyService;
        }

        public async Task<ActionResult> GetAllGames()
        {
            try
            {
                var allGames = await _historyService.GetAllGames();
                return View("AllGames",allGames);
            }
            catch (Exception e)
            {
                ErrorView errorGameView = new ErrorView();
                errorGameView.Error = e.Message;
                return View("_Error", errorGameView);
            }
        }

        public async Task<ActionResult> GetGameDetails(int id)
        {
            try
            {
                var gameDetails = await _historyService.GetGameDetails(id);
                return View("GameDetails", gameDetails);
            }
            catch (Exception e)
            {
                ErrorView errorGameView = new ErrorView();
                errorGameView.Error = e.Message;
                return View("_Error", errorGameView);
            }
        }
    }
}