﻿using System.Web.Http;
using System.Threading.Tasks;
using System;
using BlackJack.BusinessLogic.Services.Interfaces;

namespace BlackJack.WebAPI.Controllers
{
    [RoutePrefix("api/History")]
    public class HistoryController : ApiController
    {
        private readonly IHistoryService _historyService;
        public HistoryController(IHistoryService historyService)
        {
            _historyService = historyService;
        }

        [HttpGet]
        [Route("GetAllGames")]
        public async Task<IHttpActionResult> GetAllGames()
        {
            try
            {
                var allGames = await _historyService.GetAllGames();
                return Ok(allGames);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("GetGameDetails/{gameId}")]
        public async Task<IHttpActionResult> GetGameDetails(int gameId)
        {
            try
            {
                var gameDetails = await _historyService.GetGameDetails(gameId);
                return Ok(gameDetails);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
