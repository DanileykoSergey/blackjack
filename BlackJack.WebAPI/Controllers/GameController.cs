﻿using BlackJack.BusinessLogic.Enums;
using BlackJack.BusinessLogic.Exceptions;
using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.ViewModels.APIViews;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace BlackJack.WebAPI.Controllers
{
    [RoutePrefix("api/Game")]
    public class GameController : ApiController
    {
        private readonly IGameService _gameService;
        public GameController(IGameService gameService)
        {
            _gameService = gameService;
        }

        [HttpGet]
        [Route("GetStart")]
        public async Task<IHttpActionResult> GetStart()
        {
            try
            {
                var allHumanPlayers = await _gameService.GetStart();
                return Ok(allHumanPlayers);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("PlayCurrentGame/{gameId}/{getCardEnum}")]
        public async Task<IHttpActionResult> PlayCurrentGame(int gameId, GetCardEnum getCardEnum)
        {
            try
            {
                var startGame = await _gameService.PlayCurrentGame(gameId, getCardEnum);
                return Ok(startGame);

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]   
        [Route("CreateGame")]
        public async Task<IHttpActionResult> CreateGame(CreateGameGameView createGameGameView)
        {
            try
            {
                var gameId = await _gameService.CreateGame(createGameGameView.CurrentPlayer, createGameGameView.BotsCount);
                return Ok(gameId);
            }
            catch (AppValidationException e)
            {
                return BadRequest(e.Message);

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("GetEndGame/{gameId}")]
        public async Task<IHttpActionResult> GetEndGame(int gameId)
        {
            try
            {
                await _gameService.GetEndGame(gameId);
                var endGame = await _gameService.GetEndGame(gameId);
                return Ok(endGame);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
